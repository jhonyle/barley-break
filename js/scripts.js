(function () {

Array.prototype.randomValues = function (values) {

	var rand, i, j, size = Math.sqrt(values.length);
	for (i = 0; i < size; i++) {
		this[i] = [];
		for (j = 0; j < size; j++) {
			rand = Math.floor( Math.random() * values.length );
			this[i].push(values[rand]);
			values.splice(rand, 1);
		}
	}

	this[i - 1][j - 1] = 0;

	// console.log(this);

	return this;
};

var controller = function () {
	var that = {}, 
		fieldSize = 4, 
		cellSize = 40, 
		correctField = [], 
		values = [], 
		field = [], 
		nullXY = [fieldSize - 1, fieldSize - 1], 
		imgFon = new Image(),
		countStep = 0;

	imgFon.src = "img/fon.png";

	that.init = function () {
		var i, j, val = 0;
		for (var iter = 1; iter < Math.pow(fieldSize, 2); iter++) {
			values.push(iter);
		}
		for (i = 0; i < fieldSize; i++) {
			correctField[i] = [];
			for (j = 0; j < fieldSize; j++) {
				correctField[i][j] = ++val;
			}
		}
		correctField[i - 1][j - 1] = 0;

		field.randomValues(values);
		drawField();
		setMessage("Количество ходов: " + countStep);
	}

	var setMessage = function (message) {
		var div = document.getElementById("for_message");
		div.innerHTML = message;
	}

	var drawField = function () {

		var i, j, newDiv;

		var canvas =  document.getElementById("barley_break");
		var contextCanvas = canvas.getContext("2d");

		canvas.width = cellSize * fieldSize;
		canvas.height = cellSize * fieldSize;

		for (i = 0; i < fieldSize; i++) {
			for (j = 0; j < fieldSize; j++) {
				if (field[i][j] != 0) {
					contextCanvas.drawImage(imgFon, j * cellSize, i * cellSize, cellSize, cellSize);
					contextCanvas.textAlign = 'center';
					contextCanvas.fillStyle = "#00F";
				    contextCanvas.font = "italic " +  cellSize / 3 + "pt Arial";

					contextCanvas.fillText(field[i][j], j * cellSize + cellSize / 2, i * cellSize + (cellSize * 0.5 + cellSize / 6));
				}
			}
		}

		var mouseClick = function(event) {
			var realX = event.clientX-document.documentElement.scrollLeft-canvas.offsetLeft;
			var realY = event.clientY-document.documentElement.scrollTop-canvas.offsetTop;
			var x = Math.floor(realX / cellSize);
			var y = Math.floor(realY / cellSize);

			moveCell(x, y);
		}

		canvas.addEventListener("mousedown", mouseClick, false);
	}

	var endGame = function(x, y) {
		var i, j, boolValue = true;
		for (i = 0; i < fieldSize; i++) {
			for (j = 0; j < fieldSize; j++) {
				if (field[i][j] != correctField[i][j]) boolValue = false;
			}
		}
		return boolValue;
	}

	var moveCell = function(x, y) {
		if ((Math.abs(x - nullXY[0]) == 1 && Math.abs(y - nullXY[1]) == 0) || (Math.abs(y - nullXY[1]) == 1 && Math.abs(x - nullXY[0]) == 0)) {

			var canvas =  document.getElementById("barley_break");
			var contextCanvas = canvas.getContext("2d");

			var canvasMove =  document.getElementById("barley_break_move");
			var contextCanvasMove = canvas.getContext("2d");

			var pos, difference, move;

			countStep += 1;
			setMessage("Количество ходов: " + countStep);

			if (Math.abs(y  - nullXY[1]) == 0) { 
				pos = true;
				difference = x - nullXY[0];
				move = x;
			}
			else {
				pos = false;
				difference = y - nullXY[1];
				move = y;
			}

			contextCanvas.clearRect(cellSize * x, cellSize * y, cellSize, cellSize);
			canvasMove.style.display = "block";

			contextCanvasMove.textAlign = 'center';
			contextCanvasMove.fillStyle = "#00F";
			contextCanvasMove.font = "italic " +  cellSize / 3 + "pt Arial";

			var timer = setInterval(function() {

				if (pos && Math.abs(move - nullXY[0]) > 0.01) {
					if (Math.abs(move) == 1) contextCanvas.clearRect(cellSize * x, cellSize * y, cellSize, cellSize);
					else contextCanvasMove.clearRect(cellSize * move, cellSize * y, cellSize, cellSize);
					move -= difference / 10;
					contextCanvasMove.drawImage(imgFon, move * cellSize, nullXY[1] * cellSize, cellSize, cellSize);
					contextCanvasMove.fillText(field[y][x], move * cellSize + cellSize / 2, nullXY[1] * cellSize + (cellSize * 0.5 + cellSize / 6));
					
				}
				else if (!pos && Math.abs(move - nullXY[1]) > 0.01) {
					if (Math.abs(move) == 1) contextCanvas.clearRect(cellSize * x, cellSize * y, cellSize, cellSize);
					else contextCanvasMove.clearRect(cellSize * x, cellSize * move, cellSize, cellSize);
					move -= difference / 10;
					contextCanvasMove.drawImage(imgFon, nullXY[0] * cellSize, move * cellSize, cellSize, cellSize);
					contextCanvasMove.fillText(field[y][x], nullXY[0] * cellSize + cellSize / 2, move * cellSize + (cellSize * 0.5 + cellSize / 6));
				}
				else {
					
					contextCanvasMove.clearRect();
					canvasMove.style.display = "none";

					contextCanvas.drawImage(imgFon, nullXY[0] * cellSize, nullXY[1] * cellSize, cellSize, cellSize);
					contextCanvas.fillText(field[y][x], nullXY[0] * cellSize + cellSize / 2, nullXY[1] * cellSize + (cellSize * 0.5 + cellSize / 6));
					
					field[nullXY[1]][nullXY[0]] = field[y][x];			
					field[y][x] = 0;

					nullXY[0] = x;
					nullXY[1] = y;
					clearInterval(timer);
				}
			}, 30); 

			if (endGame()) alert("Вы выиграли! Решение найдено за " + countStep + " ходов");
		}
	}

	return that;
};

$(window).load(function() {
	var cont = controller();
	cont.init();
});

}());	